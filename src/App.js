import React, { Component } from 'react';
import AdminHome from './pages/admin-home';
import Faqs from './pages/faqs';
import WithdrawFromWallet from './pages/withdraw-wallet';
import AdminDashboard from './pages/admin-dashboard';
import {routes as ROUTES} from './routes';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
//import logo from './logo.svg';
//import './App.css';

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path={ROUTES.adminHome} exact={ROUTES.adminHome} component={AdminHome} />
          <Route path={ROUTES.faqs} component={Faqs} />
          <Route path={ROUTES.withdraw_wallet} component={WithdrawFromWallet} />
          <Route path={ROUTES.adminDashboard} component={AdminDashboard} />
        </Switch>
      </Router>
    )
  }
}

export default App;
