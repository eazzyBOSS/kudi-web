import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import {routes as ROUTES} from '../../../routes';
import {Link} from 'react-router-dom';
import {styles} from './styles';


function HeaderAppBar(props) {
    const { classes } = props;
    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            <IconButton className={classes.menuButton} color="inherit" aria-label="Menu">
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" color="inherit" className={classes.grow}>
              Bakudi Pay
            </Typography>
            <Button color="inherit" component={Link} to={ROUTES.faqs}>FAQs</Button>
            <Button color="inherit" component={Link} to={ROUTES.contactUs}>Contact Us</Button>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
  
  HeaderAppBar.propTypes = {
    classes: PropTypes.object.isRequired,
  };

  export default withStyles(styles)(HeaderAppBar)