import React, { Component } from 'react';
import HeaderAppBar from './header';
import AdmOverlayForm from './components/adm-overlay-form';

class AdminHome extends Component {
    constructor(props) {
        super(props);
    }

    render () {
        return (
            <div className="admin-cover-div">
                <div className="admin-cover-overlay">
                    <HeaderAppBar />
                    <div className="admin-content-div">
                        <div className="overlay-info-cover-flex">
                            <div className="overlay-info-cover-item overlay-info-text-item">
                                <div className="overlay-text-cover">
                                    <h1 className="overlay-text-h">Minimize your exposure to fraud.</h1>
                                    <p className="overlay-text-p">When you’re looking for a romantic place to spend your honeymoon, wedding anniversary or just to have some time together, go for a boutique hotel.</p>
                                </div>
                            </div>
                            <div className="overlay-info-cover-item overlay-info-form-item">
                                <div className="overlay-form-cover">
                                    <AdmOverlayForm />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default AdminHome;
