export const styles = theme => ({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    margin: {
      margin: theme.spacing.unit,
    },
    withoutLabel: {
      marginTop: theme.spacing.unit * 3,
    },
    textField: {
      flexBasis: 200,
    },
    card: {
        minWidth: 275,
        width: '90%',
        paddingTop: '30px',
        //paddingRight: '50px',
        //paddingLeft: '30px',
    },
    cardContent: {
        padding: '0px;'
    },
    title: {
        fontSize: '1.5rem',
        textAlign: 'center',
        color: '#000000',
        fontWeight: 'bold',
      },
      subtitle: {
        fontSize: '0.8rem',
        textAlign: 'center',
        color: 'grey',
        fontWeight: '400',
      },
      button: {
        margin: theme.spacing.unit,
        width: '90%',
      },
  });