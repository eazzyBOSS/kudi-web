import React, { Component } from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import {routes as ROUTES} from '../../../routes';
import {styles} from './styles';


class AdmOverlayForm extends Component {
    state = {
        amount: '',
      };    

    handleChange = prop => event => {
        this.setState({ [prop]: event.target.value });
      };
    

    render () {
        const { classes } = this.props;

        return (
            <div className={classes.root}>
                <Card className={classes.card}>
                    <CardContent className={classes.cardContent}>
                        <Typography className={classes.title} color="textSecondary" gutterBottom>
                            Login Access
                        </Typography>
                        <Typography className={classes.subtitle} color="textSecondary" gutterBottom>
                            YOUR PERSONAL LOAN PORTAL
                        </Typography>
                        <div className="form-group">
                            <FormControl fullWidth className={classNames(`${classes.margin} overlay-form-control`)}>
                                <Input
                                    disableUnderline={true}
                                    id="adornment-amount"
                                    value={this.state.amount}
                                    placeholder="Username or email"
                                    onChange={this.handleChange('amount')}
                                    startAdornment={<InputAdornment position="start">$</InputAdornment>}
                                />
                            </FormControl>
                        </div>
                        <div className="form-group">
                            <FormControl fullWidth className={classes.margin}>
                                <Input
                                    disableUnderline={true}
                                    id="adornment-amount"
                                    value={this.state.amount}
                                    placeholder="Password"
                                    onChange={this.handleChange('amount')}
                                    startAdornment={<InputAdornment position="start">$</InputAdornment>}
                                />
                            </FormControl>
                        </div>
                        <div className="button-group-flex">
                            <div className="button-group-item">
                                <p className="button-group-item-p">Forgot password?</p>
                                <Button variant="contained" color="primary" className={classes.button}
                                component={Link}
                                to={ROUTES.adminDashboard}
                                >
                                Login
                              </Button>
                            </div>
                        </div>
                    </CardContent>
                </Card>
            </div>
        )
    }
}

AdmOverlayForm.propTypes = {
    classes: PropTypes.object.isRequired,
};
  

export default withStyles(styles)(AdmOverlayForm);