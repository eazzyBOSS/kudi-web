import React, { Component } from 'react';
import CheckCircleOutlined from '@material-ui/icons/CheckCircleOutlined';
import HeaderAppBar from '../headers/admin-dashboard-header';

class WithdrawFromWallet extends Component {
    constructor(props) {
        super(props);
    }

    render () {
        return (
            <div className="withdraw-wallet-cover-div">
                <HeaderAppBar />
                <div className="withdraw-wallet-content-div">
                    <div className="withdraw-wallet-content-cover-div">
                        <div className="withdraw-wallet-content-text-cover">
                            <p class="withdraw-wallet-content-icon-p"><CheckCircleOutlined className="withdraw-wallet-content-icon" /></p>
                            <h1 className="withdraw-wallet-content-text-h">Transaction Sucessful</h1>
                            <p className="withdraw-wallet-content-text-p">You have successfully withdrawn <span className="lightblue-span">N20,000</span> from your Wallet. The amount has been sent to your registered account.</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default WithdrawFromWallet;
