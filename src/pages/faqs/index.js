import React, { Component } from 'react';
import HeaderAppBar from './header';
import {Link} from 'react-router-dom';

class Faqs extends Component {
    constructor(props) {
        super(props);
    }

    render () {
        return (
            <div className="faqs-cover-div">
                <HeaderAppBar />
                <div className="faqs-content-div">
                    <div className="faqs-content-info1-div center">
                        <div className="faqs-content-text-cover">
                            <h1 className="faqs-content-text-h">Frequently Asked Questions</h1>
                            <p className="faqs-content-text-p">Hi if you have a question that isn’t answered on this page, you can send us an email <Link to="#/">help@kuding.com</Link></p>
                        </div>
                    </div>
                    <div className="faqs-content-info1-div">
                        <div className="faqs-content-text-cover">
                            <h1 className="faqs-content-text-h">How does Kudi work?</h1>
                            <p className="faqs-content-text-p">When you’re looking for a romantic place to spend your honeymoon, wedding anniversary or just to have some time together, go for a boutique hotel. What small, chic, designer hotels don’t know about creating a romantic atmosphere isn’t worth knowing</p>
                        </div>
                    </div>
                    <div className="faqs-content-info2-div">
                        <div className="faqs-content-text-cover">
                            <h1 className="faqs-content-text-h">Minimize your exposure to fraud.</h1>
                            <p className="faqs-content-text-p">When you’re looking for a romantic place to spend your honeymoon, wedding anniversary or just to have some time together, go for a boutique hotel.</p>
                        </div>
                    </div>
                    <div className="faqs-content-info2-div">
                        <div className="faqs-content-text-cover">
                            <h1 className="faqs-content-text-h">Minimize your exposure to fraud.</h1>
                            <p className="faqs-content-text-p">When you’re looking for a romantic place to spend your honeymoon, wedding anniversary or just to have some time together, go for a boutique hotel.</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Faqs;
