import React, { Component } from 'react';
import CheckCircleOutlined from '@material-ui/icons/CheckCircleOutlined';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import {withStyles} from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import ArrowForwardOutlined from '@material-ui/icons/ArrowForwardOutlined';
import HeaderAppBar from '../headers/admin-dashboard-header';
import {styles} from './styles';
import TransactionHistoryTable from './components/transac-history-table';

class AdminDashboard extends Component {
    constructor(props) {
        super(props);
    }

    render () {
        const {classes} = this.props;
        return (
            <div className="admin-dashboard-cover-div">
                <HeaderAppBar />
                <div className="admin-dashboard-content-div">
                    <div className="admin-dashboard-content-cover-div">
                        <div className="dashboard-intro-text-container">
                            <div className="dashboard-intro-text-cover">
                                <h2 className="dashboard-intro-text-h">Hello Gbemisola, this is your Wallet Summary</h2>
                            </div>
                        </div>
                        <div className="content-dashboard-step1-container">
                            <div class="content-dashboard-cover-flex">
                                <div className="content-dashboard-cover-item content-dashboard-card-item">
                                    <Card className="content-dashboard-card">
                                        <CardContent className="content-dashboard-cardcontent">
                                            <h2 className="content-dashboard-card-title">N100,000.00</h2>
                                            <p className="content-dashboard-card-subtitle">Wallet Balance</p>
                                        </CardContent>
                                    </Card>
                                </div>
                                <div className="content-dashboard-cover-item content-dashboard-cover-btn-flex">
                                    <div className="content-dashboard-cover-btn-item">
                                        <Fab aria-label="Add" className={classes.fab} >
                                            <AddIcon />
                                        </Fab>
                                        <Fab aria-label="Add" className={classes.fab} >
                                            <ArrowForwardOutlined />
                                        </Fab>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="transaction-table-container-div">
                            <div className="transaction-table-cover-div">
                                <TransactionHistoryTable />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withStyles(styles)(AdminDashboard);
