import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Badge from '@material-ui/core/Badge';
import {styles} from './styles';

class TransactionHistoryTable extends Component {
    constructor(props) {
        super(props);
    }

    render () {
        const { classes } = this.props;
        return (
            <Paper className={classes.paperRoot}>
                <div className="transac-header-text-div">
                    <h2 className="transac-header-text-h">Transaction History</h2>
                </div>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell></TableCell>
                            <TableCell>Date</TableCell>
                            <TableCell>Amount</TableCell>
                            <TableCell>Details</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow>
                            <TableCell scope="row"><div className="status-circle-div"></div></TableCell>
                            <TableCell scope="row">20/10/20</TableCell>
                            <TableCell scope="row">N15,000.00</TableCell>
                            <TableCell scope="row">Contract payment to Jane Doe for the month of March</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell scope="row"><div className="status-circle-div"></div></TableCell>
                            <TableCell scope="row">20/10/20</TableCell>
                            <TableCell scope="row">N15,000.00</TableCell>
                            <TableCell scope="row">Contract payment to Jane Doe for the month of March</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell scope="row"><div className="status-circle-div"></div></TableCell>
                            <TableCell scope="row">20/10/20</TableCell>
                            <TableCell scope="row">N15,000.00</TableCell>
                            <TableCell scope="row">Contract payment to Jane Doe for the month of March</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell scope="row"><div className="status-circle-div"></div></TableCell>
                            <TableCell scope="row">20/10/20</TableCell>
                            <TableCell scope="row">N15,000.00</TableCell>
                            <TableCell scope="row">Contract payment to Jane Doe for the month of March</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell scope="row"><div className="status-circle-div"></div></TableCell>
                            <TableCell scope="row">20/10/20</TableCell>
                            <TableCell scope="row">N15,000.00</TableCell>
                            <TableCell scope="row">Contract payment to Jane Doe for the month of March</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell scope="row"><div className="status-circle-div"></div></TableCell>
                            <TableCell scope="row">20/10/20</TableCell>
                            <TableCell scope="row">N15,000.00</TableCell>
                            <TableCell scope="row">Contract payment to Jane Doe for the month of March</TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </Paper>
        )
    }
}

export default withStyles(styles)(TransactionHistoryTable)