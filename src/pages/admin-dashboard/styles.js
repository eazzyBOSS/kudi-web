export const styles = theme => ({
    fab: {
        margin: theme.spacing.unit,
        width: 75,
        height: 75,
        backgroundColor: '#4e92e2',
        color: 'white',
        fontSize: '3rem',
      },
})