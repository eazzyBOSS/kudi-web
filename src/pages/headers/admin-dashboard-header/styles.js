export const styles = {
    root: {
      flexGrow: 1,
    },
    grow: {
      flexGrow: 1,
    },
    menuButton: {
      marginLeft: -12,
      marginRight: 20,
    },
    avatar: {
      margin: 10,
    },
    bigAvatar: {
      margin: 10,
      width: 50,
      height: 50,
    },
  };